#coding=utf8

import socket
import urllib2
import re


def check_ip(host):
    ips = socket.gethostbyname_ex(host)[2]
    return ips

def check_myip():
    host = socket.gethostname()
    return check_ip(host)

def check_myip_internet():
    url = 'http://whereismyip.com'
    text = urllib2.urlopen(url).read()
    pat = re.compile(r'<b>.*?(\d+\.\d+\.\d+\.\d+).*?</b>', re.I)
    res = pat.search( text )
    if res:
        return res.group(1)
    return None


if __name__ == '__main__':
    print check_myip()
    print check_myip_internet()
    print check_ip('baidu.com')
